package com.example.crossprofiletest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.content.pm.CrossProfileApps
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

const val EXTRA_MESSAGE = "com.example.CrossProfileTest.MESSAGE"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra(EXTRA_MESSAGE)

        val textView1 = findViewById<TextView>(R.id.textView1)

        val cpa = getSystemService(CrossProfileApps::class.java)

        if (message != null) {
            textView1.apply {
                text = "We just got a message! Wonder who it's from...\nAnyway, it said this:\n" +
                        message
            }
        }
        else {
            textView1.apply {
                text = "targetUserProfiles: " + cpa.targetUserProfiles.joinToString() + "\n" +
                        "canInteractAcrossProfiles: " + cpa.canInteractAcrossProfiles() + "\n" +
                        "canRequestInteractAcrossProfiles: " +
                        cpa.canRequestInteractAcrossProfiles()
            }
        }

        if (!cpa.canInteractAcrossProfiles() && cpa.canRequestInteractAcrossProfiles()) {
            textView1.apply {
                append("\nRequesting permission to interact across profiles...");
            }
            val req = cpa.createRequestInteractAcrossProfilesIntent()
            startActivity(req)
        }
    }

    fun sendMessage(view: View) {
        val editText = findViewById<EditText>(R.id.editTextMessage)
        val message = editText.text.toString()

        val textView1 = findViewById<TextView>(R.id.textView1)
        textView1.apply {
            text = ""
        }

        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, message)
        }

        val cpa = getSystemService(CrossProfileApps::class.java)

        cpa.targetUserProfiles.forEach {
            textView1.apply {
                append("Sending to ${it}... ")
            }
            try {
                cpa.startActivity(intent, it, null);
                textView1.apply {
                    append("Succeeded.")
                }
            } catch (e : Exception) {
                textView1.apply {
                    append("Failed.")
                }
            }
            textView1.apply {
                append("\n")
            }
        }

        textView1.apply {
            append("Finished sending message to other profiles.")
        }
    }
}